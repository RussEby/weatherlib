/**
 * @author Russell Eby
 * Twitter @RussEby
 */

var WeatherLIB = function () {
    
    var data = {};
    
    // If the city, state is missing, request the information from Weather Underground
    var getLocation = function (location){
       $.ajax({
            url: "http://api.wunderground.com/api/"+data.apicode+"/geolookup/q/"+location.coords.latitude+","+location.coords.longitude+".json",
            dataType: "jsonp",
            success: function (parsed_json) {
                data.city = parsed_json.location.city;
                data.state = parsed_json.location.state;
                init(data);
            }
        });
    }
    
    var saveLocation = function(city, state){
        localStorage.setItem('weatherLIB', JSON.stringify({
            city: 'New York',
            state: 'NY'
        }));
    }
    
    // some browsers suck
    function storageAvailable(type) {
        try {
            var storage = window[type],
                x = '__storage_test__';
            storage.setItem(x, x);
            storage.removeItem(x);
            return true;
        }
        catch(e) {
            return false;
        }
    }
    
    // get the location from API call, localStorage, browser
    var findLocation = function() {
        
        if (data.city === ""){
            if (storageAvailable('localStorage')) {
                if(localStorage.getItem('weatherLIB')) {
                    var loco = JSON.parse(localStorage.getItem('weatherLIB'));
                    data.city = loco.city;
                    data.state = loco.state;
                } else {
                    navigator.geolocation.getCurrentPosition(getLocation);
                    return false;
                }
            } else {
                navigator.geolocation.getCurrentPosition(getLocation);
                return false;
            }
        }
        return true;
    } 

    // check for missing information and load the weather information
    var init = function (options) {

        if (options.apicode != "") {

            data.img = options.img || data.img || "";
            data.mainWeather = options.mainWeather || data.mainWeather || "";
            data.weather_Link = options.link || data.weather_Link || "";
            
            data.city = options.city || data.city || "";
            data.state = options.state || data.state || "";
            
            data.apicode = options.apicode;
            
            if (findLocation()) {
               loadWeather(data.apicode, data.state, data.city);
            }

        } else {
            return "No APICode";
        }
    }

    // Load the weather information from WeatherUnderground and call the display weather function
    var loadWeather = function (APIcode, state, city) {
        if (storageAvailable('localStorage')) {
            saveLocation(state, city);
        };
        $.ajax({
            url: "http://api.wunderground.com/api/" + APIcode + "/conditions/q/" + state + "/" + city + ".json",
            dataType: "jsonp",
            success: function (parsed_json) {
                displayWeather(parsed_json);
            }
        });
    };
    
    var pickInfo = function(weatherInfo, datum){

        datum = datum.split("!!");
                
        if (datum.length == 1){
            return weatherInfo[datum[0]];
        } else {
            return weatherInfo[datum[0]][datum[1]];
        } 
    }

    // Break up the weather information and put it into place
    var displayWeather = function (weatherInfo) {

        // put pieces into the main weather area
        $(data.mainWeather).each(function (index) {
            var element = $(this).data('weather_element');
     
            var info = pickInfo(weatherInfo["current_observation"], element);

            $(this).html(info);
        });

        // 
        $(data.img).each(function (index) {
            var element = $(this).data('weather_element');
            
            var info = pickInfo(weatherInfo["current_observation"], element);
            
            $(this).attr('src', info);
        });
        
        $(data.weather_Link).each(function (index) {
            var element = $(this).data('weather_element');
            
            var info = pickInfo(weatherInfo["current_observation"], element);
                
            $(this).attr('href', info);
        });
    };

    return {
        init: init
    }
};

