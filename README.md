# README #

This is a small JavaScript plug-in that allows quick use of the Weather Underground API. 

### How do I get set up? ###

Weather Underground requires an account be set up to receive the API code the Weather Underground requires.

JQuery is required to use this plug-in.

## Configuration

JQuery needs to be called before the weatherLib.js file.

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="script/weatherlib.js" type="text/javascript" charset="utf-8"></script>

The WeatherLIB function accepts a JavaScript object which must include the apicode, and at least one CSS class selector.

```
var wuAPICode = "__Your API Code goes here";

var weatherDisplay = new WeatherLIB;

weatherDisplay.init({apicode:wuAPICode, 
                img: '.weather_img',
                mainWeather: '.weather',
                link: '.weather_link'}
);
```

## Use

The plug-in has been set to use data elements in the HTML to determine where the data is to be put and which data values to place. Each element that is to be filled in must have a weather class. There are there different weather calles.The

* img: '.weather_img'
    * will cycle through any elements with this class and add the specified data element into the src attribute of the element
* mainWeather: '.weather'
    * will cycle through any elements with this class and add the specified data element into the html of the element
* link: '.weather_link'
    * will cycle through any elements with this class and add the specified data element into the href of the element

Each element also needs a data attribute which will specified what part of the weather underground JSON object goes with that element. If the requested data point is within and object the path is be included with a separation of `!!`.



## Location

Adding a city and state, the location can be specified during the API call. 

For example: 

```
var wuAPICode = "__Your API Code goes here";

var weatherDisplay = new WeatherLIB;

weatherDisplay.init({apicode:wuAPICode, 
                img: '.weather_img',
                mainWeather: '.weather',
                link: '.weather_link',
                city: 'Bloomington',
                state: 'MN'}
);
```

If the city and state are not providing there will be an attempt at getting the location from the browser.

## Examples

```
<p>Current Temp in F : <span class="weather" data-weather_element="temp_f"></span></p>

<p>Location is: <span class="weather" data-weather_element="city"></span>.</p>
<p>The image included is: 
<img class="weather_img" data-weather_element="icon_url" src="" alt="Weather Icon" />
</p>
```

## Future

Plans include 

- being able to pull even more of the weather information in
- Error messages for no data
- Error message for a single failure
- Error checking for the state and city.
- Add call back for failure


